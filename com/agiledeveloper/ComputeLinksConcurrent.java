package com.agiledeveloper;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

public class ComputeLinksConcurrent implements ComputeLinks {
  private ConcurrentHashMap.KeySetView<String, Boolean> visited = ConcurrentHashMap.newKeySet();
  private ExecutorService executorService;
  private AtomicLong tasksCount = new AtomicLong(0);
  private CountDownLatch latch = new CountDownLatch(1);

  public boolean visitSubLinks(String url) {
    tasksCount.incrementAndGet();
    executorService.execute(() -> {
      visited.add(url);

      LinksFinder.getLinks(url)
              .stream()
              .forEach(link -> visited.getMap().computeIfAbsent(link, this::visitSubLinks));
      if (tasksCount.decrementAndGet() == 0) latch.countDown();
    });

    return true;
  }

  public long countLinks(String url) {
    executorService = Executors.newFixedThreadPool(128);

    visitSubLinks(url);

    try {
      latch.await(1, TimeUnit.MINUTES);
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      executorService.shutdown();
    }
    return visited.size();
  }
}
